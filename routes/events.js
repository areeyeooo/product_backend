const express = require('express')
const router = express.Router()
const Event = require('../models/Event')

const getEvents = async function (req, res, next) {
  try {
    console.log(req.query)
    console.log('555555555555555555555555')
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const events = await Event.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } },
        { endDate: { $gte: startDate, $lt: endDate } }]
    }).exec()
    res.status(200).json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
router.get('/', getEvents)// GET Events
module.exports = router
